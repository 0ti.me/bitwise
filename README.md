# bitwise library

This typescript library is meant to provide bitwise and bytewise tooling with an interface allowing either function to work.

The main purpose of this is to be able to switch between a bitwise array/buffer and a bytewise array/buffer on a whim depending on configurations. For example, a PNG can use a variety of bit depths including 1 and 8. In the case of a bit depth of 1, a bit array is more useful, but in the case of a bit depth of 8, a byte array makes way more sense.
