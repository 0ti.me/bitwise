export interface IDynamicBufferOptions {
  /* the length to initialize any newly created buffer */
  bufferLength?: number;

  /* the buffer to use instead of initializing a new one */
  initialBuffer?: Buffer;
}

export interface IDynamicBuffer {
  /* props/variables */
  buffer: Buffer;
  lengthInBits: number;
  lengthInBytes: number;

  /* constructor */
  /* I think you can't do constructors in interfaces in javascript, but this is what it should look like: */
  /* constructor(opts?: IDynamicBufferOptions); */

  /* functions */

  /**
   * @param buffer: the buffer value from which to source all data
   * @param length: optionally define the length of bytes to use (default is the whole buffer length)
   * @param offset: optionally define the offset of bytes to use (default is 0)
   *
   * @returns itself for chaining
   */
  addBuffer(buffer: Buffer, length?: number, offset?: number): IDynamicBuffer;

  /**
   * Adds the least significant bits from the value
   *
   * @param val: the value from which to source least significant bits
   * @param countBits: the number of bits to use
   *
   * @returns itself for chaining
   */
  addLSBits(val: number, countBits: number): IDynamicBuffer;

  /**
   * Adds the most significant bits from the value
   *
   * @param val: the value from which to source most significant bits
   * @param countBits: the number of bits to use
   *
   * @returns itself for chaining
   */
  addMSBits(val: number, countBits: number): IDynamicBuffer;

  /**
   * @param val: the value from which to source all 8 bits (0-255)
   *
   * @returns itself for chaining
   */
  addUInt8(val: number): IDynamicBuffer;

  /**
   * @param val: the value from which to source all 16 bits (0-65535)
   *
   * @returns itself for chaining
   */
  addUInt16(val: number): IDynamicBuffer;

  /**
   * @param val: the value from which to source all 32 bits (0-4294967295)
   *
   * @returns itself for chaining
   */
  addUInt32(val: number): IDynamicBuffer;
}
