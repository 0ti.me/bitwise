import { IDynamicBuffer, IDynamicBufferOptions } from './dynamic-buffer';
import {
  LEAST_SIGNIFICANT_MASK_LIST,
  MOST_SIGNIFICANT_MASK_LIST,
} from './constants';

export class BitArray implements IDynamicBuffer {
  private mBuffer: Buffer;
  private mUnderlyingBufferLength: number;
  private mFilledBits: number;

  /**
   * The filled portion of the buffer
   */
  public get buffer(): Buffer {
    return this.mBuffer.slice(0, this.lengthInBytes);
  }

  /**
   * The length of the underlying buffer
   */
  public get bufferLength(): number {
    return this.mUnderlyingBufferLength;
  }

  /**
   * The filled length in bits
   */
  public get lengthInBits(): number {
    return this.mFilledBits;
  }

  /**
   * The filled length in bytes
   */
  public get lengthInBytes(): number {
    return Math.ceil(this.mFilledBits / 8.0);
  }

  /**
   * Constructs a bit array with the provided options
   *
   * Default a newly initialized buffer and length 4096
   *
   * @param opts: The dynamic buffer options
   */
  constructor(opts?: IDynamicBufferOptions) {
    const len = opts?.initialBuffer?.length ?? opts?.bufferLength ?? 4096;

    this.mUnderlyingBufferLength = len;
    this.mBuffer = opts?.initialBuffer ?? Buffer.alloc(len);
    this.mFilledBits = 0;
  }

  /**
   * Pushes a buffer byte for byte in its entirety into the bit array like a concatenation
   *
   * @param buf: The buffer to concatenate into the bit array
   *
   * @returns the bit array for chaining
   */
  public addBuffer(buf: Buffer): BitArray {
    let bitCount: number = 0;
    let index: number = 0;
    let uint32: number;

    /* while needs at least 4 bytes, load 4 pad bytes at a time */
    while (index + 3 < buf.length) {
      uint32 = buf.readUInt32BE(index);
      index += 4;
      this.addMSBits(uint32, 32);
    }

    bitCount = (buf.length - index) * 8;

    switch (bitCount) {
      case 24:
        /* 24 bits remaining */
        uint32 = buf.readUInt32BE(index - 1) << 8;
        break;
      case 16:
        /* 16 bits remaining */
        uint32 = buf.readUInt16BE(index) << 16;
        break;
      case 8:
        /* 8 bits remaining */
        uint32 = buf.readUInt8(index) << 24;
        break;
      case 0:
        /* 0 bits remaining */
        return this;
    }

    return this.addMSBits(uint32, bitCount);
  }

  /**
   * Writes the least significant bits to the array, limited to the count number of bits
   *
   * @param val: up to a 32-bit unsigned integer
   * @param count: the number of bits from the val to push into the bit array
   *
   * @returns the bit array for chaining
   */
  public addLSBits(val: number, count: number): BitArray {
    if (count < 0 || count > 32)
      throw new Error('sorry, limiting to 0 >= count >= 32');

    return this.addMSBits((val << (32 - count)) >>> 0, count);
  }

  /**
   * Writes the most significant bits to the array, limited to the count number of bits
   *
   * @param val: up to a 32-bit unsigned integer with the relevant (count) bits shifted all the way left if < 32
   * @param count: the number of bits to write
   *
   * @returns the bit array for chaining
   */
  public addMSBits(val: number, count: number): BitArray {
    if (count < 0 || count > 32) {
      throw new Error('sorry, limiting to 0 >= count >= 32');
    }

    let bitOffset = this.mFilledBits % 8;
    let byteOffset: number;
    let curr: number;
    let mask: number;
    let result: number;
    let wrote: number;

    while (count > 0) {
      /* recalculate the byte offset at each step */
      byteOffset = this.mFilledBits >> 3;

      curr = this.mBuffer[byteOffset];

      /* if value at offset, bit shift it left 3 bytes and coerce to UInt, else use 0 */
      if (curr) {
        curr = (curr << 24) >>> 0;
      }

      /* select a mask to pull out the most significant bits from the 32 bit int */
      mask = MOST_SIGNIFICANT_MASK_LIST[count];

      /**
       * or the new value after shifting with the current value,
       * then coerce it to unsigned by right shifting zero times
       */
      result = (curr | ((val & mask) >>> bitOffset)) >>> 0;

      /* write the 32 bit int result masked to the requested bits according to count */
      this.mBuffer.writeUInt32BE(result, byteOffset);

      /* add the number of bits written to length and subtract it from count */
      wrote = Math.min(count, 32 - bitOffset);
      this.mFilledBits += wrote;
      count -= wrote;

      /* bit shift the relevant remaining portion of the value */
      val &= LEAST_SIGNIFICANT_MASK_LIST[32 - bitOffset];
      val = (val << (32 - bitOffset)) >>> 0;

      /* after the first loop we are byte-aligned, so the bitOffset should be set to zero */
      bitOffset = 0;
    }

    return this;
  }

  /**
   * Writes the 8-bit unsigned integer provided to the buffer
   *
   * @param val: 8-bit unsigned integer
   *
   * @returns the bit array for chaining
   */
  public addUInt8(val: number): BitArray {
    return this.addMSBits((val << 24) >>> 0, 8);
  }

  /**
   * Writes the 16-bit unsigned integer provided to the buffer
   *
   * @param val: 16-bit unsigned integer
   *
   * @returns the bit array for chaining
   */
  public addUInt16(val: number): BitArray {
    return this.addMSBits((val << 16) >>> 0, 16);
  }

  /**
   * Writes the 32-bit unsigned integer provided to the buffer
   *
   * @param val: 32-bit integer to be coerced into a 32-bit unsigned integer
   *
   * @returns the bit array for chaining
   */
  public addUInt32(val: number): BitArray {
    return this.addMSBits(val >>> 0, 32);
  }

  /**
   * Artificially increases the length up to the next byte.
   *
   * @returns the bit array for chaining
   */
  public padLengthToNextByte(): BitArray {
    this.mFilledBits += (8 - (this.mFilledBits % 8)) % 8;

    return this;
  }

  public printBuffer(): BitArray {
    /* eslint-disable-next-line no-console */
    console.log(this.toString());

    return this;
  }

  public toString(): string {
    return `0x${this.buffer.toString('hex')} len (dec): ${this.lengthInBits}`;
  }
}
