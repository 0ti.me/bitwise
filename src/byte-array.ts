import { IDynamicBuffer, IDynamicBufferOptions } from './dynamic-buffer';
import {
  LEAST_SIGNIFICANT_MASK_LIST,
  MOST_SIGNIFICANT_MASK_LIST,
} from './constants';

export class ByteArray implements IDynamicBuffer {
  private mBuffer: Buffer;
  private mUnderlyingBufferLength: number;
  private mFilledBytes: number;

  /**
   * The filled portion of the buffer
   */
  public get buffer(): Buffer {
    return this.mBuffer.slice(0, this.lengthInBytes);
  }

  /**
   * The length of the underlying buffer
   */
  public get bufferLength(): number {
    return this.mUnderlyingBufferLength;
  }

  /**
   * The filled length in bits
   */
  public get lengthInBits(): number {
    return this.mFilledBytes * 8;
  }

  /**
   * The filled length in bytes
   */
  public get lengthInBytes(): number {
    return this.mFilledBytes;
  }

  /**
   * Constructs a byte array with the provided options
   *
   * Default a newly initialized buffer and length 4096
   *
   * @param opts: The dynamic buffer options
   */
  constructor(opts?: IDynamicBufferOptions) {
    const len = opts?.initialBuffer?.length ?? opts?.bufferLength ?? 4096;

    this.mUnderlyingBufferLength = len;
    this.mBuffer = opts?.initialBuffer ?? Buffer.alloc(len);
    this.mFilledBytes = 0;
  }

  /**
   * Pushes a buffer byte for byte in its entirety into the byte array like a concatenation
   *
   * @param buf: The buffer to concatenate into the byte array
   *
   * @returns the byte array for chaining
   */
  public addBuffer(buf: Buffer): ByteArray {
    buf.copy(this.mBuffer, this.lengthInBytes);

    this.mFilledBytes += buf.length;

    return this;
  }

  /**
   * Writes the least significant bits to the array, limited to the count number of bits
   *
   * @param val: an 8, 16, or 32 bit unsigned integer
   * @param count: the number of bits to add (count ∈ {8, 16, 32})
   *
   * @returns the byte array for chaining
   */
  public addLSBits(val: number, count: number): ByteArray {
    if (count !== 8 && count !== 16 && count !== 32) {
      /* \u2208 is ∈, the symbol for lhs (element) within rhs (set) */
      throw new Error('required: count \u2208 {8, 16, 32}');
    }

    val = (val & LEAST_SIGNIFICANT_MASK_LIST[count]) >>> 0;

    switch (count) {
      case 8:
        this.mBuffer.writeUInt8(val, this.lengthInBytes);
        break;
      case 16:
        this.mBuffer.writeUInt16BE(val, this.lengthInBytes);
        break;
      case 32:
        this.mBuffer.writeUInt32BE(val, this.lengthInBytes);
        break;
    }

    this.mFilledBytes += count / 8;

    return this;
  }

  /**
   * Writes the most significant bits to the array, limited to the count number of bits
   *
   * @param val: a 32-bit unsigned integer with the relevant (count) bits shifted all the way left if < 32
   * @param count: the number of bits to add (count ∈ {8, 16, 32})
   *
   * @returns the byte array for chaining
   */
  public addMSBits(val: number, count: number): ByteArray {
    if (count !== 8 && count !== 16 && count !== 32) {
      /* \u2208 is ∈, the symbol for lhs (element) within rhs (set) */
      throw new Error('required: count \u2208 {8, 16, 32}');
    }

    val = (val & MOST_SIGNIFICANT_MASK_LIST[count]) >>> 0;

    switch (count) {
      case 8:
        this.mBuffer.writeUInt8(val >>> 24, this.lengthInBytes);
        break;
      case 16:
        this.mBuffer.writeUInt16BE(val >>> 16, this.lengthInBytes);
        break;
      case 32:
        this.mBuffer.writeUInt32BE(val, this.lengthInBytes);
        break;
    }

    this.mFilledBytes += count / 8;

    return this;
  }

  /**
   * Writes the 8-bit unsigned integer provided to the buffer
   *
   * @param val: 8-bit unsigned integer
   *
   * @returns the byte array for chaining
   */
  public addUInt8(val: number): ByteArray {
    this.mBuffer.writeUInt8(val, this.lengthInBytes);

    this.mFilledBytes += 1;

    return this;
  }

  /**
   * Writes the 16-bit unsigned integer provided to the buffer
   *
   * @param val: 16-bit unsigned integer
   *
   * @returns the byte array for chaining
   */
  public addUInt16(val: number): ByteArray {
    this.mBuffer.writeUInt16BE(val, this.lengthInBytes);

    this.mFilledBytes += 2;

    return this;
  }

  /**
   * Writes the 32-bit unsigned integer provided to the buffer
   *
   * @param val: 32-bit integer to be coerced into a 32-bit unsigned integer
   *
   * @returns the byte array for chaining
   */
  public addUInt32(val: number): ByteArray {
    this.mBuffer.writeUInt32BE(val, this.lengthInBytes);

    this.mFilledBytes += 4;

    return this;
  }

  public printBuffer(): ByteArray {
    /* eslint-disable-next-line no-console */
    console.log(this.toString());

    return this;
  }

  public toString(): string {
    return `0x${this.buffer.toString('hex')} len (dec): ${this.lengthInBits}`;
  }
}
