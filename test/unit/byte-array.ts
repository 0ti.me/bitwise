import { ByteArray } from '../../src/byte-array';
import { d, expect, sinon } from '@0ti.me/ts-test-deps';

const h = (str: string): Buffer => Buffer.from(str, 'hex');

const me = __filename;

d(me, () => {
  let bkp: any;
  let byteArray: ByteArray;
  let bufLen: number;

  beforeEach(() => {
    bufLen = 128;
    bkp = global.console.log;
    global.console.log = sinon.stub();

    byteArray = new ByteArray({
      bufferLength: bufLen,
    });
  });

  afterEach(() => {
    global.console.log = bkp;
  });

  describe('constructor argument should be optional', () => {
    it('should not throw', () =>
      expect(() => (byteArray = new ByteArray())).not.to.throw());

    it('should default to 4096', () =>
      expect(new ByteArray()).to.have.property('bufferLength', 4096));
  });

  describe('given an initially provided buffer', () => {
    let fullBuffer: Buffer;
    let offsetBuffer: Buffer;

    beforeEach(() => {
      fullBuffer = Buffer.alloc(8);
      offsetBuffer = fullBuffer.slice(4, 8);
      byteArray = new ByteArray({
        initialBuffer: offsetBuffer,
      });
    });

    it('should write to the given buffer', () => {
      byteArray.addUInt32(0x11ec11ec);

      expect(offsetBuffer.toString('hex')).to.equal('11ec11ec');
      expect(fullBuffer.toString('hex')).to.equal('0000000011ec11ec');
    });
  });

  describe('given an empty byte array', () => {
    it('should be empty', () => {
      expect(byteArray.buffer).to.deep.equal(Buffer.from(''));
      expect(byteArray.lengthInBytes).to.equal(0);
      expect(byteArray.lengthInBits).to.equal(0);
    });

    it('should append an 8 bit unsigned integer', () => {
      expect(byteArray.addUInt8(0xff).buffer).to.deep.equal(h('ff'));
      expect(byteArray.lengthInBytes).to.equal(1);
      expect(byteArray.lengthInBits).to.equal(8);
    });

    it('should append a 16 bit unsigned integer', () => {
      expect(byteArray.addUInt16(0xffff).buffer).to.deep.equal(h('ffff'));
      expect(byteArray.lengthInBytes).to.equal(2);
      expect(byteArray.lengthInBits).to.equal(16);
    });

    it('should append a 32 bit unsigned integer', () => {
      expect(byteArray.addUInt32(0xffffffff).buffer).to.deep.equal(
        h('ffffffff'),
      );
      expect(byteArray.lengthInBytes).to.equal(4);
      expect(byteArray.lengthInBits).to.equal(32);
    });

    it('should append a buffer', () => {
      const b = h('ec11ec11ec11ec11');
      expect(byteArray.addBuffer(b).buffer).to.deep.equal(b);
      expect(byteArray.lengthInBytes).to.equal(8);
      expect(byteArray.lengthInBits).to.equal(64);
    });

    it('should append the least significant 8 bits', () => {
      expect(byteArray.addLSBits(0x11, 8).buffer).to.deep.equal(h('11'));
      expect(byteArray.lengthInBytes).to.equal(1);
      expect(byteArray.lengthInBits).to.equal(8);
    });

    it('should append the least significant 16 bits', () => {
      expect(byteArray.addLSBits(0xffffec11, 16).buffer).to.deep.equal(
        h('ec11'),
      );
      expect(byteArray.lengthInBytes).to.equal(2);
      expect(byteArray.lengthInBits).to.equal(16);
    });

    it('should append the least significant 32 bits', () => {
      expect(byteArray.addLSBits(0xec11ec11, 32).buffer).to.deep.equal(
        h('ec11ec11'),
      );
      expect(byteArray.lengthInBytes).to.equal(4);
      expect(byteArray.lengthInBits).to.equal(32);
    });

    it('should append the most significant 8 bits', () => {
      expect(byteArray.addMSBits(0xec11ec11, 8).buffer).to.deep.equal(h('ec'));
      expect(byteArray.lengthInBytes).to.equal(1);
      expect(byteArray.lengthInBits).to.equal(8);
    });

    it('should append the most significant 16 bits', () => {
      expect(byteArray.addMSBits(0xec11ffff, 16).buffer).to.deep.equal(
        h('ec11'),
      );
      expect(byteArray.lengthInBytes).to.equal(2);
      expect(byteArray.lengthInBits).to.equal(16);
    });

    it('should append the most significant 32 bits', () => {
      expect(byteArray.addMSBits(0xec11ec11, 32).buffer).to.deep.equal(
        h('ec11ec11'),
      );
      expect(byteArray.lengthInBytes).to.equal(4);
      expect(byteArray.lengthInBits).to.equal(32);
    });

    it('should try to log with the buffer', () => {
      expect(byteArray.printBuffer()).to.equal(byteArray);
      expect(global.console.log).to.have.been.calledOnceWithExactly(
        byteArray.toString(),
      );
    });

    it('should expose a buffer length', () => {
      expect(byteArray).to.have.property('bufferLength', bufLen);
    });

    const badCounts = new Array(34)
      /* fill everything with -1 */
      .fill(-1)
      /* initialize each item as -1 + i */
      .map((count, i) => count + i)
      /* filter out the good counts */
      .filter((count) => ![8, 16, 32].includes(count));

    it(`(addLSBits) should throw an error given a number of bits different from 8, 16, or 32`, () => {
      expect(badCounts).to.have.property('length', 31);

      /* test all the bad counts */
      badCounts.forEach((count) => {
        expect(() => byteArray.addLSBits(0, count)).to.throw();
      });
    });

    it(`(addMSBits) should throw an error given a number of bits different from 8, 16, or 32`, () => {
      expect(badCounts).to.have.property('length', 31);

      /* test all the bad counts */
      badCounts.forEach((count) => {
        expect(() => byteArray.addMSBits(0, count)).to.throw();
      });
    });
  });
});
