import { BitArray } from '../../src/bit-array';
import { d, expect, sinon } from '@0ti.me/ts-test-deps';

const h = (str: string): Buffer => Buffer.from(str, 'hex');

const me = __filename;

d(me, () => {
  let bitArray: BitArray;
  let bkp: any;
  let bufLen: number;

  beforeEach(() => {
    bufLen = 128;
    bkp = global.console.log;
    global.console.log = sinon.stub();

    bitArray = new BitArray({
      bufferLength: bufLen,
    });
  });

  afterEach(() => {
    global.console.log = bkp;
  });

  describe('buffer length constructor argument should be optional', () => {
    it('should not throw', () =>
      expect(() => (bitArray = new BitArray())).not.to.throw());

    it('should default to 4096', () =>
      expect(new BitArray()).to.have.property('bufferLength', 4096));
  });

  describe('given an initially provided buffer', () => {
    let fullBuffer: Buffer;
    let offsetBuffer: Buffer;

    beforeEach(() => {
      fullBuffer = Buffer.alloc(8);
      offsetBuffer = fullBuffer.slice(4, 8);
      bitArray = new BitArray({
        initialBuffer: offsetBuffer,
      });
    });

    it('should write to the given buffer', () => {
      bitArray.addUInt32(0x11ec11ec);

      expect(offsetBuffer.toString('hex')).to.equal('11ec11ec');
      expect(fullBuffer.toString('hex')).to.equal('0000000011ec11ec');
    });
  });

  describe('given a bit offset of zero', () => {
    it('should append an eight bit integer and only have that in the buffer', () => {
      expect(bitArray.addUInt8(0xec).buffer).to.deep.equal(h('ec'));
      expect(bitArray).to.have.property('lengthInBits', 8);
    });

    it('should append a sixteen bit integer and only have that in the buffer', () => {
      expect(bitArray.addUInt16(0xec11).buffer).to.deep.equal(h('ec11'));
      expect(bitArray).to.have.property('lengthInBits', 16);
    });

    it('should append a thirty-two bit integer and only have that in the buffer', () => {
      expect(bitArray.addUInt32(0xec11ec11).buffer).to.deep.equal(
        h('ec11ec11'),
      );
      expect(bitArray).to.have.property('lengthInBits', 32);
    });

    it('should error with a least significant bits count less than zero', () => {
      expect(() => bitArray.addLSBits(0, -1)).to.throw();
    });

    it('should error with a most significant bits count less than zero', () => {
      expect(() => bitArray.addMSBits(0, -1)).to.throw();
    });

    it('should error with a least significant bits count greater than thirty-two', () => {
      expect(() => bitArray.addLSBits(0, 33)).to.throw();
    });

    it('should error with a most significant bits count greater than thirty-two', () => {
      expect(() => bitArray.addMSBits(0, 33)).to.throw();
    });
  });

  describe('given a bit offset of one', () => {
    beforeEach(() => {
      expect(bitArray.addMSBits(0xff000000, 1).buffer).to.deep.equal(h('80'));
    });

    it('should append an eight bit integer and have nine bits in the buffer', () => {
      expect(bitArray.addUInt8(0xec).buffer).to.deep.equal(h('f600'));
      expect(bitArray).to.have.property('lengthInBits', 9);
    });

    it('should append an eight bit integer and values should flow into the ninth bit', () => {
      expect(bitArray.addUInt8(0x01).buffer).to.deep.equal(h('8080'));
      expect(bitArray).to.have.property('lengthInBits', 9);
    });

    it('should append a sixteen bit integer and have seventeen bits in the buffer', () => {
      expect(bitArray.addUInt16(0xec11).buffer).to.deep.equal(h('f60880'));
      expect(bitArray).to.have.property('lengthInBits', 17);
    });

    it('should pad up to 8 bits', () => {
      expect(bitArray.padLengthToNextByte()).to.have.property(
        'lengthInBits',
        8,
      );
    });

    it('should append a thirty-two bit integer and have thirty-three bits in the buffer', () => {
      expect(bitArray.addUInt32(0xec11ec11).buffer).to.deep.equal(
        h('f608f60880'),
      );
      expect(bitArray).to.have.property('lengthInBits', 33);
    });

    it('should add a buffer of length 4', () => {
      expect(bitArray.addBuffer(h('ec11ec11')).buffer).to.deep.equal(
        h('f608f60880'),
      );
    });

    it('should add a buffer of length 5', () => {
      expect(bitArray.addBuffer(h('ec11ec11ec')).buffer).to.deep.equal(
        h('f608f608f600'),
      );
    });

    it('should add a buffer of length 6', () => {
      expect(bitArray.addBuffer(h('ec11ec11ec11')).buffer).to.deep.equal(
        h('f608f608f60880'),
      );
    });

    it('should add a buffer of length 7', () => {
      expect(bitArray.addBuffer(h('ec11ec11ec11ec')).buffer).to.deep.equal(
        h('f608f608f608f600'),
      );
    });

    it('should expose a buffer length', () => {
      expect(bitArray).to.have.property('bufferLength', bufLen);
    });
  });

  describe('given a bit offset of six', () => {
    beforeEach(() => {
      expect(bitArray.addMSBits(0xff000000, 6).buffer).to.deep.equal(h('fc'));
    });

    it('should append an eight bit integer and have fourteen bits in the buffer', () => {
      expect(bitArray.addUInt8(0xec).buffer).to.deep.equal(h('ffb0'));
      expect(bitArray).to.have.property('lengthInBits', 14);
    });

    it('should append a sixteen bit integer and have twenty-two bits in the buffer', () => {
      expect(bitArray.addUInt16(0xec11).buffer).to.deep.equal(h('ffb044'));
      expect(bitArray).to.have.property('lengthInBits', 22);
    });

    it('should append a thirty-two bit integer and have thirty-eight bits in the buffer', () => {
      expect(bitArray.addUInt32(0xec11ec11).buffer).to.deep.equal(
        h('ffb047b044'),
      );
      expect(bitArray).to.have.property('lengthInBits', 38);
    });
  });

  describe('given an offset of eight', () => {
    beforeEach(() => {
      expect(bitArray.addMSBits(0xff000000, 8).buffer).to.deep.equal(h('ff'));
    });

    it('should append an eight bit integer and have sixteen bits in the buffer', () => {
      expect(bitArray.addUInt8(0xec).buffer).to.deep.equal(h('ffec'));
      expect(bitArray).to.have.property('lengthInBits', 16);
    });

    it('should append a sixteen bit integer and have twenty-four bits in the buffer', () => {
      expect(bitArray.addUInt16(0xec11).buffer).to.deep.equal(h('ffec11'));
      expect(bitArray).to.have.property('lengthInBits', 24);
    });

    it('should append a thirty-two bit integer and have forty bits in the buffer', () => {
      expect(bitArray.addUInt32(0xec11ec11).buffer).to.deep.equal(
        h('ffec11ec11'),
      );
      expect(bitArray).to.have.property('lengthInBits', 40);
    });
  });

  describe('given an offset of nine', () => {
    beforeEach(() => {
      expect(bitArray.addMSBits(0xffff0000, 9).buffer).to.deep.equal(h('ff80'));
    });

    it('should append an eight bit integer and have seventeen bits in the buffer', () => {
      expect(bitArray.addUInt8(0xec).buffer).to.deep.equal(h('fff600'));
      expect(bitArray).to.have.property('lengthInBits', 17);
    });

    it('should append a sixteen bit integer and have twenty-five bits in the buffer', () => {
      expect(bitArray.addUInt16(0xec11).buffer).to.deep.equal(h('fff60880'));
      expect(bitArray).to.have.property('lengthInBits', 25);
    });

    it('should append a thirty-two bit integer and have forty-one bits in the buffer', () => {
      expect(bitArray.addUInt32(0xec11ec11).buffer).to.deep.equal(
        h('fff608f60880'),
      );
      expect(bitArray).to.have.property('lengthInBits', 41);
    });
  });

  describe('given an offset of eleven', () => {
    beforeEach(() => {
      expect(bitArray.addMSBits(0xffff0000, 11).buffer).to.deep.equal(
        h('ffe0'),
      );
    });

    it('should append eleven bits for a total of twenty-two in the buffer', () => {
      expect(bitArray.addMSBits(0xecec0000, 11).buffer).to.deep.equal(
        h('fffd9c'),
      );
      expect(bitArray).to.have.property('lengthInBits', 22);
    });
  });

  describe('given an offset of fourteen', () => {
    beforeEach(() => {
      expect(bitArray.addMSBits(0xffff0000, 14).buffer).to.deep.equal(
        h('fffc'),
      );
    });

    it('should append an eight bit integer and have twenty-two bits in the buffer', () => {
      expect(bitArray.addUInt8(0xec).buffer).to.deep.equal(h('ffffb0'));
      expect(bitArray).to.have.property('lengthInBits', 22);
    });

    it('should append a sixteen bit integer and have thirty bits in the buffer', () => {
      expect(bitArray.addUInt16(0xec11).buffer).to.deep.equal(h('ffffb044'));
      expect(bitArray).to.have.property('lengthInBits', 30);
    });

    it('should append a thirty bit integer and have the expected forty-four bits in the buffer', () => {
      expect(bitArray.addLSBits(0x0000000c, 30).buffer).to.deep.equal(
        h('fffc000000c0'),
      );
      expect(bitArray).to.have.property('lengthInBits', 44);
    });

    it('should append a thirty-two bit integer and have forty-six bits in the buffer', () => {
      expect(bitArray.addUInt32(0xec11ec11).buffer).to.deep.equal(
        h('ffffb047b044'),
      );
      expect(bitArray).to.have.property('lengthInBits', 46);
    });
  });

  describe('given an offset of sixteen', () => {
    beforeEach(() => {
      expect(bitArray.addMSBits(0xffff0000, 16).buffer).to.deep.equal(
        h('ffff'),
      );
    });

    it('should append an eight bit integer and have twenty-four bits in the buffer', () => {
      expect(bitArray.addUInt8(0xec).buffer).to.deep.equal(h('ffffec'));
      expect(bitArray).to.have.property('lengthInBits', 24);
    });

    it('should append a sixteen bit integer and have thirty-two bits in the buffer', () => {
      expect(bitArray.addUInt16(0xec11).buffer).to.deep.equal(h('ffffec11'));
      expect(bitArray).to.have.property('lengthInBits', 32);
    });

    it('should append a thirty-two bit integer and have forty-eight bits in the buffer', () => {
      expect(bitArray.addUInt32(0xec11ec11).buffer).to.deep.equal(
        h('ffffec11ec11'),
      );
      expect(bitArray).to.have.property('lengthInBits', 48);
    });
  });

  describe('given an offset of seventeen', () => {
    beforeEach(() => {
      expect(bitArray.addMSBits(0xffffffff, 17).buffer).to.deep.equal(
        h('ffff80'),
      );
    });

    it('should append an eight bit integer and have twenty-five bits in the buffer', () => {
      expect(bitArray.addUInt8(0xec).buffer).to.deep.equal(h('fffff600'));
      expect(bitArray).to.have.property('lengthInBits', 25);
    });

    it('should append a sixteen bit integer and have thirty-three bits in the buffer', () => {
      expect(bitArray.addUInt16(0xec11).buffer).to.deep.equal(h('fffff60880'));
      expect(bitArray).to.have.property('lengthInBits', 33);
    });

    it('should append a thirty-two bit integer and have forty-nine bits in the buffer', () => {
      expect(bitArray.addUInt32(0xec11ec11).buffer).to.deep.equal(
        h('fffff608f60880'),
      );
      expect(bitArray).to.have.property('lengthInBits', 49);
    });
  });

  describe('given an offset of twenty-two', () => {
    beforeEach(() => {
      expect(bitArray.addMSBits(0xffffffff, 22).buffer).to.deep.equal(
        h('fffffc'),
      );
    });

    it('should append an eight bit integer and have thirty bits in the buffer', () => {
      expect(bitArray.addUInt8(0xec).buffer).to.deep.equal(h('ffffffb0'));
      expect(bitArray).to.have.property('lengthInBits', 30);
    });

    it('should append a sixteen bit integer and have thirty-eight bits in the buffer', () => {
      expect(bitArray.addUInt16(0xec11).buffer).to.deep.equal(h('ffffffb044'));
      expect(bitArray).to.have.property('lengthInBits', 38);
    });

    it('should append a thirty-two bit integer and have fifty-four bits in the buffer', () => {
      expect(bitArray.addUInt32(0xec11ec11).buffer).to.deep.equal(
        h('ffffffb047b044'),
      );
      expect(bitArray).to.have.property('lengthInBits', 54);
    });
  });

  describe('given an offset of twenty-four', () => {
    beforeEach(() => {
      expect(bitArray.addMSBits(0xffffffff, 24).buffer).to.deep.equal(
        h('ffffff'),
      );
    });

    it('should append an eight bit integer and have thirty-two bits in the buffer', () => {
      expect(bitArray.addUInt8(0xec).buffer).to.deep.equal(h('ffffffec'));
      expect(bitArray).to.have.property('lengthInBits', 32);
    });

    it('should append a sixteen bit integer and have forty bits in the buffer', () => {
      expect(bitArray.addUInt16(0xec11).buffer).to.deep.equal(h('ffffffec11'));
      expect(bitArray).to.have.property('lengthInBits', 40);
    });

    it('should append a thirty-two bit integer and have fifty-six bits in the buffer', () => {
      expect(bitArray.addUInt32(0xec11ec11).buffer).to.deep.equal(
        h('ffffffec11ec11'),
      );
      expect(bitArray).to.have.property('lengthInBits', 56);
    });
  });

  describe('given an offset of twenty-five', () => {
    beforeEach(() => {
      expect(bitArray.addMSBits(0xffffffff, 25).buffer).to.deep.equal(
        h('ffffff80'),
      );
    });

    it('should append an eight bit integer and have thirty-three bits in the buffer', () => {
      expect(bitArray.addUInt8(0xec).buffer).to.deep.equal(h('fffffff600'));
      expect(bitArray).to.have.property('lengthInBits', 33);
    });

    it('should append a sixteen bit integer and have forty-one bits in the buffer', () => {
      expect(bitArray.addUInt16(0xec11).buffer).to.deep.equal(
        h('fffffff60880'),
      );
      expect(bitArray).to.have.property('lengthInBits', 41);
    });

    it('should append a thirty-two bit integer and have fifty-seven bits in the buffer', () => {
      expect(bitArray.addUInt32(0xec11ec11).buffer).to.deep.equal(
        h('fffffff608f60880'),
      );
      expect(bitArray).to.have.property('lengthInBits', 57);
    });
  });

  describe('given an offset of thirty', () => {
    beforeEach(() => {
      expect(bitArray.addMSBits(0xffffffff, 30).buffer).to.deep.equal(
        h('fffffffc'),
      );
    });

    it('should append an eight bit integer and have thirty-eight bits in the buffer', () => {
      expect(bitArray.addUInt8(0xec).buffer).to.deep.equal(h('ffffffffb0'));
      expect(bitArray).to.have.property('lengthInBits', 38);
    });

    it('should append a sixteen bit integer and have forty-six bits in the buffer', () => {
      expect(bitArray.addUInt16(0xec11).buffer).to.deep.equal(
        h('ffffffffb044'),
      );
      expect(bitArray).to.have.property('lengthInBits', 46);
    });

    it('should append a thirty-two bit integer and have sixty-two bits in the buffer', () => {
      expect(bitArray.addUInt32(0xec11ec11).buffer).to.deep.equal(
        h('ffffffffb047b044'),
      );
      expect(bitArray).to.have.property('lengthInBits', 62);
    });
  });

  describe('given an offset of thirty-two', () => {
    beforeEach(() => {
      expect(bitArray.addMSBits(0xffffffff, 32).buffer).to.deep.equal(
        h('ffffffff'),
      );
    });

    it('should append an eight bit integer and have forty bits in the buffer', () => {
      expect(bitArray.addUInt8(0xec).buffer).to.deep.equal(h('ffffffffec'));
      expect(bitArray).to.have.property('lengthInBits', 40);
    });

    it('should append a sixteen bit integer and have forty-eight bits in the buffer', () => {
      expect(bitArray.addUInt16(0xec11).buffer).to.deep.equal(
        h('ffffffffec11'),
      );
      expect(bitArray).to.have.property('lengthInBits', 48);
    });

    it('should append a thirty-two bit integer and have sixty-four bits in the buffer', () => {
      expect(bitArray.addUInt32(0xec11ec11).buffer).to.deep.equal(
        h('ffffffffec11ec11'),
      );
      expect(bitArray).to.have.property('lengthInBits', 64);
    });
  });

  describe('given an offset of thirty-three', () => {
    beforeEach(() => {
      expect(
        bitArray.addMSBits(0xffffffff, 32).addMSBits(0xff000000, 1).buffer,
      ).to.deep.equal(h('ffffffff80'));
    });

    it('should append an eight bit integer and have forty-one bits in the buffer', () => {
      expect(bitArray.addUInt8(0xec).buffer).to.deep.equal(h('fffffffff600'));
      expect(bitArray).to.have.property('lengthInBits', 41);
    });

    it('should append a sixteen bit integer and have forty-nine bits in the buffer', () => {
      expect(bitArray.addUInt16(0xec11).buffer).to.deep.equal(
        h('fffffffff60880'),
      );
      expect(bitArray).to.have.property('lengthInBits', 49);
    });

    it('should append a thirty-two bit integer and have sixty-five bits in the buffer', () => {
      expect(bitArray.addUInt32(0xec11ec11).buffer).to.deep.equal(
        h('fffffffff608f60880'),
      );
      expect(bitArray).to.have.property('lengthInBits', 65);
    });

    it('should try to log with the buffer, extended out to the next byte', () => {
      expect(bitArray.printBuffer()).to.equal(bitArray);
      expect(global.console.log).to.have.been.calledOnceWithExactly(
        bitArray.toString(),
      );
    });
  });

  describe('given an offset of thirty-eight', () => {
    beforeEach(() => {
      expect(
        bitArray.addMSBits(0xffffffff, 32).addMSBits(0xff000000, 6).buffer,
      ).to.deep.equal(h('fffffffffc'));
    });

    it('should append an eight bit integer and have forty-six bits in the buffer', () => {
      expect(bitArray.addUInt8(0xec).buffer).to.deep.equal(h('ffffffffffb0'));
      expect(bitArray).to.have.property('lengthInBits', 46);
    });

    it('should append a sixteen bit integer and have fifty-four bits in the buffer', () => {
      expect(bitArray.addUInt16(0xec11).buffer).to.deep.equal(
        h('ffffffffffb044'),
      );
      expect(bitArray).to.have.property('lengthInBits', 54);
    });

    it('should append a thirty-two bit integer and have seventy bits in the buffer', () => {
      expect(bitArray.addUInt32(0xec11ec11).buffer).to.deep.equal(
        h('ffffffffffb047b044'),
      );
      expect(bitArray).to.have.property('lengthInBits', 70);
    });
  });
});
